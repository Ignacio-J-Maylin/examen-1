package com.examen1.data;
/*
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.examen1.models.dtos.EmpleadoDTO;
import com.examen1.models.dtos.EmpleadosPorProvinciaDTO;
import com.examen1.models.dtos.NombreYTelefonoDTO;
import com.examen1.repository.DomiciliosRepository;
import com.examen1.repository.EmpleadosRepository;
import com.examen1.repository.TelefonosRepository;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
public class RepositoryTest {

	@Autowired
	private EmpleadosRepository repositoryEmpleados;
	@Autowired
	private DomiciliosRepository repositoryDomisilios;
	@Autowired
	private TelefonosRepository telefonosRepository;

	@Test
	public void deveriaTraerTodosLosEmpleadosDTO() {

		Iterable<EmpleadoDTO> resultado = repositoryEmpleados.findAllEmpleadoDTO();

		int esperado = 4;
		assertThat(resultado).hasSize(esperado);
		System.out.println("deveriaTraerTodosLosEmpleadosDTO");
		resultado.forEach(r -> System.out.println(r));
	}

	@Test
	public void deveriaTraerLasProvinciasLosCodigosPostalesDeLosDomiciliosDeLosEmpleadosQueGananMasQueElSueldoPromedio() {
		Iterable<String> resultado = repositoryDomisilios
				.getLasProvinciasDeLosCodigosPostalesDeLosDomiciliosDeLosEmpleadosQueGananMasQueElSueldoPromedio();
		int esperado = 1;
		// no trae repetidos por eso es un resultado
		assertThat(resultado).hasSize(esperado);
		System.out.println("deveriaTraerLasProvinciasLosCodigosPostalesDeLosDomiciliosDeLosEmpleadosQueGananMasQueElSueldoPromedio");
		resultado.forEach(r -> System.out.println(r));
	}

	@Test
	public void deveriaTraerCantidadDeEmpleadosPorProvincia() {
		Iterable<EmpleadosPorProvinciaDTO> resultado = repositoryDomisilios.getCantidadDeEmpleadosPorProvincia();
		int esperado = 3;
		assertThat(resultado).hasSize(esperado);
		System.out.println("deveriaTraerCantidadDeEmpleadosPorProvincia");
		resultado.forEach(r -> System.out.println(r));
		// El empleado Pedro Perez trabaja en dos domicilios uno en Cordoba y otro que
		// no tenemos los datos de la provincia.
		// El resto trabaja a pesar de tener 2 domicilios son en la misma Provincia.
		// Salvo uno que no tiene domicilio
	}

	@Test
	public void deveriaTraerNombresEmpleadosSinDomisilioRegistadoYTelefonoQueEmpieceCon9() {
		Iterable<NombreYTelefonoDTO> resultado = telefonosRepository
				.getNombresEmpleadosSinDomisilioRegistadoYTelefonoQueEmpieceCon9();
		int esperado = 1;
		assertThat(resultado).hasSize(esperado);
		System.out.println("deveriaTraerNombresEmpleadosSinDomisilioRegistadoYTelefonoQueEmpieceCon9");
		resultado.forEach(r -> System.out.println(r));
	}

}*/