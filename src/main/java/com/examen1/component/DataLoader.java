package com.examen1.component;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.examen1.models.entitys.CodigosPostales;
import com.examen1.models.entitys.Domicilios;
import com.examen1.models.entitys.Empleados;
import com.examen1.models.entitys.Telefonos;
import com.examen1.repository.CodigosPostalesRepository;
import com.examen1.repository.DomiciliosRepository;
import com.examen1.repository.EmpleadosRepository;
import com.examen1.repository.TelefonosRepository;

@Component
public class DataLoader implements ApplicationRunner {

	@Autowired
    private EmpleadosRepository empleadosRepository;
	@Autowired
    private TelefonosRepository telefonosRepository;
	@Autowired
    private CodigosPostalesRepository codigosPostalesRepository;
	@Autowired
    private DomiciliosRepository domiciliosRepository;

   

	@Override
    public void run(ApplicationArguments args)  throws Exception {
		
		Empleados antonioArjona = new Empleados("Antonio Arjona", "12345678A", (double) 5000);
		Empleados carlotaCerezo = new Empleados("Carlota Cerezo", "12345678c",(double) 1000);
		Empleados lauraLopez = new Empleados("Laura López", "12345678L",(double) 1500);
		Empleados pedroPerez = new Empleados("Pedro Pérez", "12345678P",(double) 2000);

		CodigosPostales c08050 = new CodigosPostales("08050","Parets","Barcelona");
		CodigosPostales c14200 = new CodigosPostales("14200","Peñarroya","Córdoba");
		CodigosPostales c14900 = new CodigosPostales("14900","Lucena","Córdoba");
		CodigosPostales c28040 = new CodigosPostales("28040","Madrid","Madrid");
		CodigosPostales c50008 = new CodigosPostales("50008","Zaragoza","Zaragoza");
		CodigosPostales c28004 = new CodigosPostales("28004","Arganda","Madrid");
		CodigosPostales c28000 = new CodigosPostales("28000","Madrid","Madrid");
		CodigosPostales c15200 = new CodigosPostales("15200","no se sabe","no se sabe");

		Telefonos tCarlotaCerezo  = new Telefonos(carlotaCerezo,611111111);
		Telefonos t2CarlotaCerezo = new Telefonos(carlotaCerezo,931111111);
		Telefonos tLauraLopez     = new Telefonos(lauraLopez,913333333);
		Telefonos tPedroPerez     = new Telefonos(pedroPerez,913333333);
		Telefonos t2pedroPerez     = new Telefonos(pedroPerez,644444444);

		Domicilios dAntonioArjona = new Domicilios(antonioArjona,"Avda. Complutense",c28040);
		Domicilios d2AntonioArjona = new Domicilios(antonioArjona,"Cántaro",c28004);
		Domicilios dPedroPerez = new Domicilios(pedroPerez,"Diamante",c15200);
		Domicilios d2PedroPerez = new Domicilios(pedroPerez,"Carbón",c14900);
		Domicilios dLauraLopez = new Domicilios(lauraLopez,"Diamante",c14200);

		HashSet<Empleados> empleados = new HashSet<Empleados>();
		empleados.add(antonioArjona);
		empleados.add(carlotaCerezo);
		empleados.add(lauraLopez);
		empleados.add(pedroPerez);

		HashSet<CodigosPostales> codigosPostales = new HashSet<CodigosPostales>();
		codigosPostales.add(c08050);
		codigosPostales.add(c14200);
		codigosPostales.add(c14900);
		codigosPostales.add(c28040);
		codigosPostales.add(c50008);
		codigosPostales.add(c28004);
		codigosPostales.add(c28000);
		codigosPostales.add(c15200);

		HashSet<Telefonos> telefonos = new HashSet<Telefonos>();
		telefonos.add(tCarlotaCerezo);
		telefonos.add(t2CarlotaCerezo);
		telefonos.add(tLauraLopez);
		telefonos.add(tPedroPerez);
		telefonos.add(t2pedroPerez);

		HashSet<Domicilios> domicilios = new HashSet<Domicilios>();
		domicilios.add(dAntonioArjona);
		domicilios.add(d2AntonioArjona);
		domicilios.add(dPedroPerez);
		domicilios.add(d2PedroPerez);
		domicilios.add(dLauraLopez);
		
		
		
		empleadosRepository.saveAll(empleados);
		
		codigosPostalesRepository.saveAll(codigosPostales);
		 
		telefonosRepository.saveAll(telefonos);
		
		domiciliosRepository.saveAll(domicilios);
    }

}