package com.examen1.models.entitys;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "DOMICILIOS")
public class Domicilios {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID", nullable=false, unique=true)
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DNI")
	private Empleados dNI;
	@Column(name = "CALLE")
	private String calle;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CODIGO_POSTAL")
	private CodigosPostales codigoPostal;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Empleados getdNI() {
		return dNI;
	}
	public void setdNI(Empleados dNI) {
		this.dNI = dNI;
	}
	public CodigosPostales getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(CodigosPostales codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	
	public Domicilios() {
	}
	public Domicilios(Empleados empleado, String calle, CodigosPostales codigosPostales) {
		this.dNI = empleado;
		this.calle = calle;
		this.codigoPostal = codigosPostales;
	}
	
	
	
}
