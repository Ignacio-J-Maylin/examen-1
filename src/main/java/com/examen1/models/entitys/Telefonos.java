package com.examen1.models.entitys;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TELEFONOS")
public class Telefonos {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID", nullable=false, unique=true)
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DNI")
	private Empleados dNI;
	@Column(name = "TELEFONO",nullable = false)
	private Integer telefono;
	public Empleados getdNI() {
		return dNI;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setdNI(Empleados dNI) {
		this.dNI = dNI;
	}
	public Integer getTelefono() {
		return telefono;
	}
	public void setTelefono(Integer telefono) {
		this.telefono = telefono;
	}
	public Telefonos(Empleados dNI, Integer telefono) {
		this.dNI = dNI;
		this.telefono = telefono;
	}
	public Telefonos() {
	}
	
	
	
}
