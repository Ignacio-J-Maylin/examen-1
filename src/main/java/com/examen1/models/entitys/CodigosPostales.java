package com.examen1.models.entitys;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "CODIGOS_POSTALES")
public class CodigosPostales {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID", nullable=false, unique=true)
	private Long id;
	@Column(name = "CODIGO_POSTAL", nullable = false, unique = true)
	private String codigoPostal;
	@Column(name = "POBLACION",nullable = false)
	private String poblacion;
	@Column(name = "PROVINCIA",nullable = false)
	private String provincia;
	@OneToMany(mappedBy = "codigoPostal", fetch = FetchType.LAZY)
	private List<Domicilios> domicilios = new ArrayList<Domicilios>();
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public String getPoblacion() {
		return poblacion;
	}
	public void setPoblacion(String poblacion) {
		this.poblacion = poblacion;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public List<Domicilios> getDomicilios() {
		return domicilios;
	}
	public void setDomicilios(List<Domicilios> domicilios) {
		this.domicilios = domicilios;
	}
	
	public CodigosPostales() {
	}
	public CodigosPostales(String codigoPostal, String poblacion, String provincia) {
		this.codigoPostal = codigoPostal;
		this.poblacion = poblacion;
		this.provincia = provincia;
	}
	
	
}
