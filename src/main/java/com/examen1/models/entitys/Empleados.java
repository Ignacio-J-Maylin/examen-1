package com.examen1.models.entitys;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "EMPLEADOS")
public class Empleados {


	@Column(name = "NOMBRE",nullable = false)
	private String nombre;
	@Id
	@Column(name = "DNI",nullable = false, unique=true)
	private String dNI;
	@Column(name = "SUELDO",nullable = false)
	private Double sueldo;
	@OneToMany(mappedBy = "dNI", fetch = FetchType.LAZY)
	private List<Telefonos> telefonos = new ArrayList<Telefonos>();
	@OneToMany(mappedBy = "dNI", fetch = FetchType.LAZY)
	private List<Domicilios> domicilios = new ArrayList<Domicilios>();
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getdNI() {
		return dNI;
	}
	public void setdNI(String dNI) {
		this.dNI = dNI;
	}
	public Double getSueldo() {
		return sueldo;
	}
	public void setSueldo(Double sueldo) {
		this.sueldo = sueldo;
	}
	public List<Telefonos> getTelefonos() {
		return telefonos;
	}
	public void setTelefonos(List<Telefonos> telefonos) {
		this.telefonos = telefonos;
	}
	public List<Domicilios> getDomicilios() {
		return domicilios;
	}
	public void setDomicilios(List<Domicilios> domicilios) {
		this.domicilios = domicilios;
	}
	public Empleados() {
	}
	public Empleados(String nombre, String dNI, Double sueldo) {
	this.nombre = nombre;
	this.dNI = dNI;
	this.sueldo = sueldo;
	}
	@Override
	public String toString() {
		return "Empleados [nombre=" + nombre + ", dNI=" + dNI + ", sueldo=" + sueldo + "]";
	}
	
}
