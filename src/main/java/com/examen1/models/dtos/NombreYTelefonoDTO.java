package com.examen1.models.dtos;

public class NombreYTelefonoDTO {

	private String nombreEmpleado;
	private Integer telefonoEmpleado;
	public String getNombreEmpleado() {
		return nombreEmpleado;
	}
	public void setNombreEmpleado(String nombreEmpleado) {
		this.nombreEmpleado = nombreEmpleado;
	}
	public Integer getTelefonoEmpleado() {
		return telefonoEmpleado;
	}
	public void setTelefonoEmpleado(Integer telefonoEmpleado) {
		this.telefonoEmpleado = telefonoEmpleado;
	}
	public NombreYTelefonoDTO(String nombreEmpleado, Integer telefonoEmpleado) {
		super();
		this.nombreEmpleado = nombreEmpleado;
		this.telefonoEmpleado = telefonoEmpleado;
	}
	@Override
	public String toString() {
		return "NombreYTelefonoDTO [nombreEmpleado=" + nombreEmpleado + ", telefonoEmpleado=" + telefonoEmpleado + "]";
	}
	
	
}
