package com.examen1.models.dtos;

public class EmpleadosPorProvinciaDTO {

	private String provincia;
	private Long cantidadDeEmpleados;
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public Long getCantidadDeEmpleados() {
		return cantidadDeEmpleados;
	}
	public void setCantidadDeEmpleados(Long cantidadDeEmpleados) {
		this.cantidadDeEmpleados = cantidadDeEmpleados;
	}
	public EmpleadosPorProvinciaDTO(String provincia, Long cantidadDeEmpleados) {
		super();
		this.provincia = provincia;
		this.cantidadDeEmpleados = cantidadDeEmpleados;
	}
	public EmpleadosPorProvinciaDTO() {
	}
	@Override
	public String toString() {
		return "EmpleadosPorProvinciaDTO [provincia=" + provincia + ", cantidadDeEmpleados=" + cantidadDeEmpleados
				+ "]";
	}
	
	
}
