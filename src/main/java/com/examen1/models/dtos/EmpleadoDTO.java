package com.examen1.models.dtos;

import com.examen1.models.entitys.Domicilios;
import com.examen1.models.entitys.Empleados;
import com.examen1.models.entitys.Telefonos;

/**
 * @author Usuario
 *
 */
public class EmpleadoDTO {
	
	
	private String nombre;
	private String dNI;
	private String calle;
	private String poblacion;
	private String provincia;
	private String codigoPostal;
	private String telefono;
	private  int x;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getdNI() {
		return dNI;
	}
	public void setdNI(String dNI) {
		this.dNI = dNI;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getPoblacion() {
		return poblacion;
	}
	public void setPoblacion(String poblacion) {
		this.poblacion = poblacion;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	public EmpleadoDTO(Empleados e) {
		this.nombre = e.getNombre();
		this.dNI = e.getdNI();
		this.calle="";
		this.poblacion="";
		this.provincia="";
		this.codigoPostal="";
		this.telefono="";
		this.x=0;
		e.getDomicilios().forEach(d->convertirCalle(d));
		this.x=0;
		e.getDomicilios().forEach(d->convertirPoblacion(d));
		this.x=0;
		e.getDomicilios().forEach(d->convertirProvincia(d));
		this.x=0;
		e.getDomicilios().forEach(d->convertCodigoPostal(d));
		this.x=0;
		e.getTelefonos().forEach(t->convertirTelefono(t));
	}
	private void convertirTelefono(Telefonos t) {
		this.x++;
		this.telefono +=  "Telefono "+this.x+": "  +t.getTelefono() +". ";
	}
	private void convertCodigoPostal(Domicilios d) {
		this.x++;
		this.provincia +=  "domicilio "+this.x+": "  +d.getCodigoPostal().getCodigoPostal() +". ";
	}
	private void convertirProvincia(Domicilios d) {
		this.x++;
		this.provincia +=  "domicilio "+this.x+": "  +d.getCodigoPostal().getProvincia() +". ";
	}
	private void convertirPoblacion(Domicilios d) {
		this.x++;
		this.poblacion +=  "domicilio "+this.x+": "  +d.getCodigoPostal().getPoblacion() +". ";
	}
	private void convertirCalle(Domicilios d) {
		this.x++;
		this.calle +=  "domicilio "+x+": "  +d.getCalle() +". ";
	}
	@Override
	public String toString() {
		return "EmpleadoDTO [nombre=" + nombre + ", dNI=" + dNI + ", calle=" + calle + ", poblacion=" + poblacion
				+ ", provincia=" + provincia + ", codigoPostal=" + codigoPostal + ", telefono=" + telefono + "]";
	}

}
