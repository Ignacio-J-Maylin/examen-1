package com.examen1.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.examen1.exceptions.ResourceNotFoundException;
import com.examen1.models.dtos.EmpleadoDTO;
import com.examen1.models.dtos.EmpleadosPorProvinciaDTO;
import com.examen1.models.dtos.NombreYTelefonoDTO;
import com.examen1.service.ApiService;

@RestController
@RequestMapping("/api")
public class ApiRestController {

	@Autowired
	private ApiService apiSerice;

	

	@GetMapping("/pregunta1")
	public ResponseEntity<List<EmpleadoDTO>> getListadoEmpleados() throws ResourceNotFoundException {
		List<EmpleadoDTO> listaDeEmpleadosDTO = apiSerice.getListadoEmpleados();
		return ResponseEntity.ok().body(listaDeEmpleadosDTO);
	}
	

	@GetMapping("/pregunta2")
	public ResponseEntity<Set<String>> getListadoProvinciasDeLosEmpleadosQueGananMasQueElPromedio() throws ResourceNotFoundException {
		Set<String> ResultadoDeProvincias = apiSerice.getListadoProvinciasDeLosEmpleadosQueGananMasQueElPromedio();
		return ResponseEntity.ok().body(ResultadoDeProvincias);
	}
	
	@GetMapping("/pregunta3")
	public ResponseEntity<List<EmpleadosPorProvinciaDTO>> getCantidadDeEmpleadosPorProvincia() throws ResourceNotFoundException {
		List<EmpleadosPorProvinciaDTO> cantidadDeEmpleadosPorProvicia = apiSerice.getCantidadDeEmpleadosPorProvincia();
		return ResponseEntity.ok().body(cantidadDeEmpleadosPorProvicia);
	}
	

	
	@GetMapping("/pregunta4")
	public ResponseEntity<List<NombreYTelefonoDTO>> getNombresEmpleadosSinDomisilioRegistadoYTelefonoQueEmpieceCon9() throws ResourceNotFoundException {
		List<NombreYTelefonoDTO> ResultadoNombres = apiSerice.getNombresEmpleadosSinDomisilioRegistadoYTelefonoQueEmpieceCon9();
		return ResponseEntity.ok().body(ResultadoNombres);
	}
}
