package com.examen1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.examen1.models.dtos.NombreYTelefonoDTO;
import com.examen1.models.entitys.Telefonos;


@Repository
public interface TelefonosRepository extends JpaRepository<Telefonos, String>{
	
	
	
	@Query("SELECT  new com.examen1.models.dtos.NombreYTelefonoDTO( t.dNI.nombre,t.telefono) from Telefonos t where t.dNI.domicilios IS EMPTY and str(t.telefono) like '9%'")
	 List<NombreYTelefonoDTO> getNombresEmpleadosSinDomisilioRegistadoYTelefonoQueEmpieceCon9();


}
