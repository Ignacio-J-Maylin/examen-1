package com.examen1.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.examen1.models.dtos.EmpleadosPorProvinciaDTO;
import com.examen1.models.entitys.Domicilios;


@Repository
public interface DomiciliosRepository extends JpaRepository<Domicilios, String>{

	@Query("SELECT d from Domicilios d where d.dNI = (SELECT e.dNI FROM Empleados e where e.sueldo >(SELECT AVG(sueldo) as rating FROM Empleados )) ")
	List<Domicilios> getLosDomiciliosDeLosEmpleadosQueGananMasQueElSueldoPromedio();
	@Query("SELECT d.codigoPostal.provincia from Domicilios d where d.dNI = (SELECT e.dNI FROM Empleados e where e.sueldo >(SELECT AVG(sueldo) as rating FROM Empleados )) ")
	Set<String> getLasProvinciasDeLosCodigosPostalesDeLosDomiciliosDeLosEmpleadosQueGananMasQueElSueldoPromedio();

	@Query("SELECT  new com.examen1.models.dtos.EmpleadosPorProvinciaDTO(d.codigoPostal.provincia , count(distinct d.dNI))"
			+ " from Domicilios d group by  d.codigoPostal.provincia")
	List<EmpleadosPorProvinciaDTO> getCantidadDeEmpleadosPorProvincia();
}