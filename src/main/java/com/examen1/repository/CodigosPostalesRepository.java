package com.examen1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.examen1.models.entitys.CodigosPostales;



@Repository
public interface CodigosPostalesRepository extends JpaRepository<CodigosPostales, String>{


}