package com.examen1.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.examen1.models.dtos.EmpleadoDTO;
import com.examen1.models.entitys.Empleados;


@Repository
public interface EmpleadosRepository extends JpaRepository<Empleados, String>{

	
	@Query("SELECT new com.examen1.models.dtos.EmpleadoDTO(e) FROM Empleados e order by e.nombre ASC")
	List<EmpleadoDTO> findAllEmpleadoDTO();


}