package com.examen1.service.impl;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.examen1.models.dtos.EmpleadoDTO;
import com.examen1.models.dtos.EmpleadosPorProvinciaDTO;
import com.examen1.models.dtos.NombreYTelefonoDTO;
import com.examen1.repository.DomiciliosRepository;
import com.examen1.repository.EmpleadosRepository;
import com.examen1.repository.TelefonosRepository;
import com.examen1.service.ApiService;
@Service
public class ApiServiceImpl implements ApiService {

	@Autowired
    private EmpleadosRepository empleadosRepository;

	@Autowired
    private DomiciliosRepository domiciliosRepository;
	
	@Autowired
    private TelefonosRepository telefonosRepository;
	
	@Override
	public List<EmpleadoDTO> getListadoEmpleados() {
		List<EmpleadoDTO> listadoEmpleadosDTO = empleadosRepository.findAllEmpleadoDTO();
		return listadoEmpleadosDTO;
	}

	@Override
	public Set<String> getListadoProvinciasDeLosEmpleadosQueGananMasQueElPromedio() {
		
		return domiciliosRepository.getLasProvinciasDeLosCodigosPostalesDeLosDomiciliosDeLosEmpleadosQueGananMasQueElSueldoPromedio();
	}

	@Override
	public List<EmpleadosPorProvinciaDTO> getCantidadDeEmpleadosPorProvincia() {
		return  domiciliosRepository.getCantidadDeEmpleadosPorProvincia();
	}

	@Override
	public List<NombreYTelefonoDTO> getNombresEmpleadosSinDomisilioRegistadoYTelefonoQueEmpieceCon9() {
		return telefonosRepository.getNombresEmpleadosSinDomisilioRegistadoYTelefonoQueEmpieceCon9();
	}

}
