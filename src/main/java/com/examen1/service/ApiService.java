package com.examen1.service;

import java.util.List;
import java.util.Set;

import com.examen1.models.dtos.EmpleadoDTO;
import com.examen1.models.dtos.EmpleadosPorProvinciaDTO;
import com.examen1.models.dtos.NombreYTelefonoDTO;

public interface ApiService {

	List<EmpleadoDTO> getListadoEmpleados();

	Set<String> getListadoProvinciasDeLosEmpleadosQueGananMasQueElPromedio();

	List<EmpleadosPorProvinciaDTO> getCantidadDeEmpleadosPorProvincia();

	List<NombreYTelefonoDTO> getNombresEmpleadosSinDomisilioRegistadoYTelefonoQueEmpieceCon9();



}
