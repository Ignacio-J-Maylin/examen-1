package com.examen1.exceptions;

import java.util.Date;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<?> resourceNotFoundException(ResourceNotFoundException ex, WebRequest request) {
         ErrorDetails errorDetails = new ErrorDetails(HttpStatus.NOT_FOUND,new Date(), ex.getMessage(), request.getDescription(false));
         return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> globleExcpetionHandler(Exception ex, WebRequest request) {
        ErrorDetails errorDetails = new ErrorDetails(HttpStatus.INTERNAL_SERVER_ERROR,new Date(), ex.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    @ExceptionHandler
    public ResponseEntity<?> validationException(MethodArgumentNotValidException ex, WebRequest request) {
    	StringBuilder erros= new StringBuilder("");
    	 ex.getBindingResult().getAllErrors().forEach(er->erros.append(er.getDefaultMessage()));
        ErrorDetails errorDetails = new ErrorDetails(HttpStatus.EXPECTATION_FAILED,new Date(),erros.toString(), request.getDescription(false));
        return new ResponseEntity<>(errorDetails, HttpStatus.EXPECTATION_FAILED);
    }
    @ExceptionHandler
    public ResponseEntity<?> preConditionFailed(PreConditionFailedException ex, WebRequest request) {
    	ErrorDetails errorDetails = new ErrorDetails(HttpStatus.PRECONDITION_FAILED,new Date(), ex.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(errorDetails, HttpStatus.PRECONDITION_FAILED);
    }
    @ExceptionHandler
    public ResponseEntity<?> lockedException(LockedException ex, WebRequest request) {
    	ErrorDetails errorDetails = new ErrorDetails(HttpStatus.PRECONDITION_FAILED,new Date(), ex.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(errorDetails, HttpStatus.PRECONDITION_FAILED);
    }
}