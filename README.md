# Examen-1

Resolución de examen en Spring Boot 

Examen terminado:

Subido el proyecto a una imagen docker  ignaciomaylin/examen1:1.0

Para correr el proyecto tenes dos opciones:

1- descargandote el proyecto del repositorio de git lav y correrlo con una variable de entorno de java11 o superior
2- (si tenes docker) solo te descargas el archivo de docker-compose lo corres y listo.


1)como hago la opcion uno(necesitas variable de entorno java 11 a mas).
  Asi tambien, necesitas tener mySql corriendo en el puerto 3600 
  con una base de datos llamada "examen1" porque esta configurado asi el propertis
  
  -Colonas el proyecto completo. 
  -lo abris en tu idea
  -lo levantas o corres los test
  -si lo levantas podes importar el archivo examen.postman_collection.json en tu
   postman y tenes para probar los entry points (asegurate de tener el postmanAgent
   levantado para poder hacer solicitudes locales, Asi tambien el puerto 8082 libre 
   para levantar el proyecto)

2)Como hago la opcion dos (suponiendo que tenes docker en tu maquina) 


 -descargate el solo archivo docker-compose.yam de la carpeta principal.
 -desde la terminal donde se encuentra ese archivo que descargaste deveras correr los siguientes comandos
         
         :docker-compose up -d
         // una vez que te los descargo las imagenes y las corrio en contenedores
         //comprovar que esten levantados
         :docker ps 
         // si solo se levanto uno volve a correr ":docker-compose up -d"
         // una ves que esten los dos levantados podes probar a travez de postman
         // Descargate el archivo examen.postman_collection.json de la carpeta principal
         // Abri tu postman y importalo 
         // antes de probar comprova que los 2 contenedores esten corriendo
         // se conectan a travez de tu puerto 8082 por lo cual debe de estar libre
         // asegurate de tener el postmanAgent abierto para hacer solicitudes locales
         // desde el postman de tu navegador
         